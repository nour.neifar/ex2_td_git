
#ifndef __FONCTIONS_BASIQUES_H__
#define __FONCTIONS_BASIQUES_H__

extern int pgcd (int, int);
extern int ppcm (int, int);
extern int* diviseurs (int);

#endif /* __FONCTIONS_BASIQUES_H__ */
