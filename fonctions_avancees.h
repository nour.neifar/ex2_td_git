
#ifndef __FONCTIONS_AVANCEES_H__
#define __FONCTIONS_AVANCEES_H__

extern int somme_diviseurs (int);
extern int parfait (int);
extern int est_premier (int);

#endif /* __FONCTIONS_AVANCEES_H__ */
